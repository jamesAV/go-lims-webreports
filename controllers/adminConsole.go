package controllers

import (
	"net/http"
)

func (c *Controller) AdminConsole(w http.ResponseWriter, r *http.Request) {
	t := loadDefaultTemplate()
	data := map[string]interface{}{
		"MainHeader": "Admin Console",
		"Content": "Place Holder",
	}


	t.ExecuteTemplate(w,"layout",data)
}
