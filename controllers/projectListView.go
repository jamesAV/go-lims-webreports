package controllers

import (
	"net/http"
	"html/template"
	"bitbucket.org/AV-james/go-lims-webreports/model"
)

func (c *Controller) ProjectList(w http.ResponseWriter, r *http.Request) {

	//t, _ := template.ParseFiles("templates/layout.tmpl","templates/navigation.tmpl","templates/header.tmpl","templates/footer.tmpl")
	t := loadDefaultTemplate()
	data := map[string]interface{}{
		"MainHeader": "Projects",
		"Content": template.HTML(model.ServeAllProjects()),
	}


	t.ExecuteTemplate(w,"layout",data)
}

//func (c *Controller) LayoutTest(w http.ResponseWriter, r *http.Request) {
//	//t, _ := template.ParseFiles("templates/layout.tmpl")
//	t, _ := template.ParseFiles("templates/layout.tmpl","templates/navigation.tmpl","templates/header.tmpl","templates/footer.tmpl")
//	d := Layout{
//		MainHeader : "hi",
//		Content : "This is the content",
//	}
//	t.ExecuteTemplate(w,"layout",d)
//}
