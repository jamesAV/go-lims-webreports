package controllers

import (
	"bitbucket.org/AV-james/go-lims-webreports/model"
	"database/sql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func (c *Controller) GetSample(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	log.Println(params)
	sid, err := strconv.Atoi(params["sid"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid sample number")
		return
	}
	s := model.Sample{SampleNumber: sid}
	//sid := s.Ctx.Input.Param(":sampleid")
	log.Printf("sid: %v\n", sid)

	if err = s.GetSample(); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Sample not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}
	respondWithJSON(w, http.StatusOK, s)
}

func (c *Controller) GetAllSamplesFromProject(w http.ResponseWriter, r *http.Request) {
	// Get all samples from database. (limit returned number?)
	params := mux.Vars(r)
	//log.Println(params)
	spid := params["spid"]
	//sid := s.Ctx.Input.Param(":sampleid")
	log.Printf("spid: %v\n", spid)
	sampleList, err := model.GetAllSamplesFromProject(spid)
	if err != nil {
		respondWithError(w, http.StatusNotFound, "Samples not found")
		return
	}
	respondWithJSON(w, http.StatusOK, sampleList)

}
