package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"html/template"
	"io/ioutil"
)

type Context struct {
	Request        *http.Request
	ResponseWriter *http.ResponseWriter
}

type Controller struct {
	// context data
	//Ctx  *Context
	Data map[interface{}]interface{}
	DB   *sql.DB
	//A    *app.App
}

func (c *Controller) Index(w http.ResponseWriter, r *http.Request) {
	// Serve a list of routes?
	//fileName := "templates/index.html"
	log.Println("loading Index")
	//http.ServeFile(w, r, fileName)
	t := loadDefaultTemplate()
	// read in the old Index page place holder as "content" for the new index page using the templates
	buf,_ :=ioutil.ReadFile("templates/Index.html")
	data := map[string]interface{}{
		"MainHeader": "Home Page",
		"Content": template.HTML(string(buf)),
	}
	t.ExecuteTemplate(w,"layout",data)
}


func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func loadDefaultTemplate() (*template.Template){
	t, _ := template.ParseFiles("templates/layout.tmpl","templates/navigation.tmpl","templates/header.tmpl","templates/footer.tmpl")
	return t
}