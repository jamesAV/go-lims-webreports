package main_test

import (
	"bitbucket.org/AV-james/go-lims-webreports/app"
	"github.com/solf1re2/config"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a app.App

func TestMain(m *testing.M) {
	cfg := config.LoadConfig("./test_config.json")
	//log.Println(cfg)
	a := app.App{}
	a.Initialize(cfg.Server.DBHostname, cfg.Server.DBUserName, cfg.Server.DBPassword, cfg.Server.DBDatabase)

	code := m.Run()

	os.Exit(code)
}

func Test(t *testing.T) {

	req, _ := http.NewRequest("GET", "/api/samples/602919", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	log.Println(response.Body.String())

}

//func TestGetNonExistentSample(t *testing.T) {
//
//	req, _ := http.NewRequest("GET", "/api/samples/11", nil)
//	response := executeRequest(req)
//
//	checkResponseCode(t, http.StatusNotFound, response.Code)
//
//	var m map[string]string
//	json.Unmarshal(response.Body.Bytes(), &m)
//	if m["error"] != "Product not found" {
//		t.Errorf("Expected the 'error' key of the response to be set to 'Product not found'. Got '%s'", m["error"])
//	}
//}
//
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}
