FROM golang

ADD . /go/src/bitbucket.org/AV-james/go-lims-webreports
WORKDIR /go/src/bitbucket.org/AV-james/go-lims-webreports

#COPY ./ /go/src/bitbucket.org/AV-james/go-lims-webreports
#WORKDIR /go/src/bitbucket.org/AV-james/go-lims-webreports

#RUN git clone https://bitbucket.org/AV-james/go-lims-webreports
#COPY ./configs/ /go/src/bitbucket.org/AV-james/go-lims-webreports/configs
RUN go get -u -v ./
#RUN go build

RUN go get github.com/codegangsta/gin

CMD	gin run main.go

EXPOSE 3000