package main

import (
	_ "github.com/denisenkom/go-mssqldb"
	"github.com/solf1re2/config"

	"bitbucket.org/AV-james/go-lims-webreports/app"
	"html/template"
	"log"

	"bitbucket.org/AV-james/go-lims-webreports/database"
	"fmt"
)

type Page struct {
	Title      string
	RawContent string
	Content    template.HTML
	Date       string
	GUID       string
}

func main() {
	// Load the config file - PORT,
	appCfg := config.LoadServerConfig("./configs/config.json")
	dbCfg := config.LoadServerConfig("./configs/dbConfig.json")
	log.Println(appCfg)
	a := app.App{}
	database.DatabaseConnect(dbCfg.Hostname, dbCfg.UserName, dbCfg.Password, dbCfg.Database)
	a.Initialize(dbCfg.Hostname, dbCfg.UserName, dbCfg.Password, dbCfg.Database)
	fmt.Println(appCfg.Port)
	a.Run(":" + fmt.Sprintf("%v", appCfg.Port))
	//
	//makeFileOfSamples(dbConn)
}
