package database

import (
	"database/sql"
	"fmt"
	"log"
)

//type DB struct {
//	Connection *sql.DB
//}
var SQLsvr *sql.DB

func DatabaseConnect(serverName, user, password, dbname string) {
	var err error
	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s",
		serverName,
		user,
		password,
		dbname)
	//defer Connection.Close()

	SQLsvr, err = sql.Open("mssql", connectionString)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func GetDBConnection() *sql.DB {
	return SQLsvr
}
