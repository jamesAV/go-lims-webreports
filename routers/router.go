package routers

import (
	"bitbucket.org/AV-james/go-lims-webreports/controllers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func SetRoutes(r *mux.Router, ct *controllers.Controller) {

	// Webpage endpoints
	r.HandleFunc("/", ct.Index)
	r.HandleFunc("/project-list", ct.ProjectList)
	//r.HandleFunc("/layoutTest", ct.LayoutTest)

	// Admin monitoring information
	r.HandleFunc("/admin",ct.AdminConsole)


	//r.HandleFunc("/api/samples", ct.GetAll).
	//	Methods("GET")


	// API endpoints
	r.HandleFunc("/api/samples/{sid:[0-9\\-]+}", ct.GetSample).
		Methods("GET")
	r.HandleFunc("/api/samples/project/{spid:[0-9a-zA-Z\\-]+}", ct.GetAllSamplesFromProject).
		Methods("GET")
	r.HandleFunc("/api/test", testServe).
		Methods("GET")
	if r != nil {
		log.Println("Router setup: Good")
	}
}

func testServe(w http.ResponseWriter, r *http.Request) {
	fileName := "files/404.html"
	log.Println("loading Test File")
	http.ServeFile(w, r, fileName)
}

//func ServeIndex(w http.ResponseWriter, r *http.Request) {
//	var Pages = []Page{}
//	pages, err := database.Query("SELECT page_title, page_content,page_date,page_guid FROM pages ORDER BY ? DESC", "page_date")
//	if err != nil {
//		fmt.Fprintln(w, err.Error())
//	}
//	defer pages.Close()
//	for pages.Next() {
//		thisPage := Page{}
//		pages.Scan(&thisPage.Title, &thisPage.RawContent, &thisPage.Date, &thisPage.GUID)
//		thisPage.Content = template.HTML(thisPage.RawContent)
//		Pages = append(Pages, thisPage)
//	}
//	t, _ := template.ParseFiles("templates/index.html")
//	t.Execute(w, Pages)
//}
