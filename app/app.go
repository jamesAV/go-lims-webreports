package app

import (
	"bitbucket.org/AV-james/go-lims-webreports/controllers"
	"bitbucket.org/AV-james/go-lims-webreports/routers"
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type App struct {
	Router     *mux.Router
	DB         *sql.DB
	Controller *controllers.Controller
}

func (a *App) Initialize(serverName, user, password, dbname string) {
	dbConnectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s",
		serverName,
		user,
		password,
		dbname)

	var err error
	a.DB, err = sql.Open("mssql", dbConnectionString)
	if err != nil {
		log.Fatal(err.Error())
	}
	//defer a.DB.Close()
	//err = a.DB.Ping()
	//if err != nil {
	//	log.Fatal(err.Error())
	//}
	a.Router = mux.NewRouter()
	a.initializeControllers()
	a.initializeRoutes()
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func (a *App) initializeRoutes() {
	routers.SetRoutes(a.Router, a.Controller)
}
func (a *App) initializeControllers() {
	a.Controller = &controllers.Controller{}
	a.Controller.DB = a.DB
}
