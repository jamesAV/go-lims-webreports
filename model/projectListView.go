package model

import (
	"bitbucket.org/AV-james/go-lims-webreports/database"
	"log"
	"errors"
)

type ProjectListData struct {
	MainHeader string
	ProjectList []Project
}

func ServeAllProjects() (string) {
	projects, _ := GetAllProjects()
	projectList := `
	<table class="table table-striped">
	<thead class="thead-dark">
	<tr>
	<th scope="col">Project Name</th>
	<th scope="col">Target/Description</th>
	<th scope="col">Owner/Creator</th>
	</tr>
	</thead>
	<tbody>`

	for _,p := range projects {
		projectList = projectList + `<td><a href="/api/samples/project/` + p.Name + `" class="alert-link">` + p.Name + `</a></td>
		<td>` + p.Target + `</td>
		<td>` + p.Owner + `</td>
		</tr>`
	}
	projectList = projectList + `
	</tbody>
	</table>`
	return projectList
}

func GetAllProjects() ([]Project, error) {
	// Get all samples from database. (limit returned number?)

	var projectList []Project
	rows, err := database.SQLsvr.Query("SELECT name, description, owner from PROJECT where template = 'CUSTOM_AFFIMER' ")

	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("model.projectListView: Project Query failed.")
	}

	for rows.Next() {
		p := Project{}

		rows.Scan(&p.Name, &p.Target, &p.Owner)

		//fmt.Println(sample)
		if err != nil {
			return nil, err
		}
		projectList = append(projectList, p)
	}
	return projectList, nil
}