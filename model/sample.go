package model

import (
	"errors"
	"fmt"
	"log"

	"bitbucket.org/AV-james/go-lims-webreports/database"
)

type Sample struct {
	SampleNumber   int    `json:"sample_number" db:"sample_number"`
	TextId         string `json:"text_id" db:"text_id"`
	Description    string `json:"description" db:"description"`
	Project        string `json:"project" db:"project"`
	ParentSample   int    `json:"parent_sample" db:"parent_sample"`
	OriginalSample int    `json:"original_sample" db:"original_sample"`
}

func CountSamples() {
	var sampleCount int

	err := database.SQLsvr.QueryRow("SELECT count(sample_number) FROM sample WHERE template =?1", "PAN").Scan(&sampleCount)
	if err != nil {
		log.Println(err.Error())
	}
	fmt.Println(sampleCount)
}

func (s *Sample) GetSample() (err error) {

	log.Println(s.SampleNumber)

	database.SQLsvr.QueryRow("SELECT sample_number, text_id, description, project, parent_sample, original_sample from SAMPLE where sample_number =?1",
		s.SampleNumber).Scan(&s.SampleNumber, &s.TextId, &s.Description, &s.Project, &s.ParentSample, &s.OriginalSample)

	//fmt.Println(sample)
	if err != nil {
		log.Println(err.Error())
	}
	if err != nil {
		return errors.New("model.sample: Sample does not exist.")
	}
	return nil
}

func GetAllSamplesFromProject(project string) ([]Sample, error) {
	// Get all samples from database. (limit returned number?)

	var sampleList []Sample
	rows, err := database.SQLsvr.Query("SELECT sample_number, text_id, description, project, parent_sample, original_sample from SAMPLE where project =?1 order by sample_number", project)

	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("model.sample: Samples from project do not exist.")
	}

	for rows.Next() {
		s := Sample{}

		rows.Scan(&s.SampleNumber, &s.TextId, &s.Description, &s.Project, &s.ParentSample, &s.OriginalSample)

		//fmt.Println(sample)
		if err != nil {
			return nil, err
		}
		sampleList = append(sampleList, s)
	}
	return sampleList, nil
}

//func GetAllSamples() (*map[int]*Sample, error) {
//	// Get all samples from database. (limit returned number?)
//
//	dbConn := DatabaseConnect(DbConnectionString)
//	defer dbConn.Close()
//	rows, err := dbConn.Query("SELECT top(100) sample_number, text_id, description, project from SAMPLE order by sample_number desc")
//
//	if err != nil {
//		log.Println(err.Error())
//		return nil, errors.New("model.sample: Samples from project do not exist.")
//	}
//
//	for rows.Next() {
//		var samNum int
//		var textId string
//		var description string
//		var project string
//		//var parentSample int
//		//var originalSample int
//		var sample Sample
//		rows.Scan(&samNum, &textId, &description, &project)
//
//		sample = Sample{samNum, textId, description, project}
//
//		if err != nil {
//			log.Println(err.Error())
//		}
//		SampleList[samNum] = &sample
//	}
//	return &SampleList, nil
//}
