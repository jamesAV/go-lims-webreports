package model

type plate struct {
	PlateName        string `json:"plate_name" db:"name"`
	PlateTemplate    string `json:"template" db:"template"`
	PlateDescription string `json:"description" db:"description"`
	PlatePositions   map[string]*platePosition
}

type platePosition struct {
	PositionID       string  `json:"position_id" db:"position_id"`
	Concentration    float32 `json:"concentration" db:"concentration"`
	ParentPlate      string  `json:"c_parent_plate" db:"c_parent_plate"`
	ParentPositionID string  `json:"c_parent_well" db:"c_parent_well"`
	Sample           *Sample
}

func GetPlatePosition() {

}

func GetPlate() {

}
