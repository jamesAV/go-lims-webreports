go-lims-webreports
## run Dockerfile to build
```
docker build ./ -t lims-web
```
## run container with exposed to port 3000 (gin) 
```<<image ID>>``` is obtained from the end of the build output.

```
docker run -p 8080:3000 -v %GOPATH%\src\bitbucket.org\AV-james\go-lims-webreports:/go/src/bitbucket.org/AV-james/go-lims-webreports lims-web
```

## remove dangling images
```docker rmi $(docker images -f dangling=true -q)```

## cleanup of docker images (maybe to build from scratch)
stop docker container and remove
```
 docker stop $(docker ps -a -q)
 docker rm $(docker ps -a -q)
 docker rmi $(docker images -a -q)
```


##run with gin
- gin - live reload utility [https://github.com/codegangsta/gin]


```gin run main.go```

```
   $gin -h
   NAME:
      gin - A live reload utility for Go web applications.
   
   USAGE:
      gin.exe [global options] command [command options] [arguments...]
   
   VERSION:
      0.0.0
   
   COMMANDS:
        run, r   Run the gin proxy in the current working directory
        env, e   Display environment variables set by the .env file
        help, h  Shows a list of commands or help for one command
   
   GLOBAL OPTIONS:
      --laddr value, -l value       listening address for the proxy server
      --port value, -p value        port for the proxy server (default: 3000)
      --appPort value, -a value     port for the Go web server (default: 3001)
      --bin value, -b value         name of generated binary file (default: "gin-bin")
      --path value, -t value        Path to watch files from (default: ".")
      --build value, -d value       Path to build files from (defaults to same value as --path)
      --excludeDir value, -x value  Relative directories to exclude
      --immediate, -i               run the server immediately after it's built
      --all                         reloads whenever any file changes, as opposed to reloading only on .go file change
      --godep, -g                   use godep when building
      --buildArgs value             Additional go build arguments
      --certFile value              TLS Certificate
      --keyFile value               TLS Certificate Key
      --help, -h                    show help
      --version, -v                 print the version
   ```
